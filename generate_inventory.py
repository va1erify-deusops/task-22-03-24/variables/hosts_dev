import json
import sys

def create_inventory(json_file):
    try:
        # Открываем JSON-файл
        with open(json_file, 'r') as f:
            data = json.load(f)

        # Создаем файл inventory для Ansible
        with open('hosts', 'w') as file:
            file.write('[dev]\n')  # Группа хостов
            for host_key, host_data in data["vm_info"]["value"].items():
                file.write(f'{host_key} ansible_host={host_data["ip"]}\n')  # Информация о хосте

        print("Inventory файл успешно создан!")
    except FileNotFoundError:
        print("Файл не найден.")
    except Exception as e:
        print(f"Произошла ошибка: {e}")

# Получаем путь к JSON-файлу из аргументов командной строки
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Использование: python3 script.py путь_к_файлу.json")
    else:
        json_file_path = sys.argv[1]
        create_inventory(json_file_path)
